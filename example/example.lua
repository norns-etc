-- An example script for norns-etc
-- Load this file in emacs and M-x compile to upload.
local MusicUtil = require "musicutil"
engine.name = "PolyPerc"

scale = nil

function redraw()
  screen.clear()
  screen.move(64, 32)
  screen.text_center(scale.name)
  screen.update()
end


function init()
  -- random scale output, just to emphasize the point
  local base = 60
  scale = MusicUtil.SCALES[math.random(1, #MusicUtil.SCALES)]
  redraw()

  local notes = {}
  for _, interval in pairs(scale.intervals) do
    table.insert(notes, base + interval)
  end

  local m = midi.connect(2)
  local c = metro.init()
  c.time = 0.2
  local on = false
  c.event = function ()
    if #notes == 0 then
      c:stop()
    elseif not on then
      on = true
      local here = notes[1]
      engine.hz(MusicUtil.note_num_to_freq(here))
      m:note_on(here, 20)
    else
      on = false
      local here = table.remove(notes, 1)
      m:note_off(here, 20)
    end
  end
  m:note_on(note, 20)
  c:start()
end

-- This needs to be at the end so that it doesn't conflict with the
-- norns convention of having a script description in comments on the
-- first couple of lines.

-- Local Variables:
-- compile-command: "make upload"
-- End:
