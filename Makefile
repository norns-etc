CFLAGS=-Werror -Wall -ggdb -O3 $(shell pkg-config --cflags nanomsg) 
LDFLAGS=$(shell pkg-config --libs nanomsg)

all: norns_shell norns_load norns_upload

install: norns_shell norns_load norns_upload
	install -D --mode=755 norns_shell /usr/local/bin/
	install -D --mode=755 norns_upload /usr/local/bin/
	install -D --mode=755 norns_load /usr/local/bin/

.PHONY: all install
